import javafx.application.Application;

/**
 * Main class with method main.
 */
public class Main {
    /**
     * Main entry point of application.
     * @param args  users input
     */
    public static void main(String[] args) {
        Application.launch(GUI.class);
    }
}
