import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;

/**
 * Class which represent GUI
 */
public class GUI extends Application {
    private TextArea textArea;
    private Stage primaryStage;
    private Text fileStatus;
    private static final int WIDTH = 420;
    private static final int HEIGHT = 510;
    private static final int BUTTON_WIDTH = 100;
    private static final int TEXT_FIELD_WIDTH = 120;
    private static final int PADDING = 10;
    private static final int SPACING = 20;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("KIV/BIT - A17B0303P");
        primaryStage.setScene(createScene());
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            System.exit(0);
        });

    }

    /**
     * Creates the scene for the window.
     * @return the scene
     */
    private Scene createScene() {
        Scene scene = new Scene(getRoot(), WIDTH, HEIGHT);
        scene.getStylesheets().add("style.css");
        return scene;
    }


    /**
     * Creates the main border pane where all the components are stored
     * @return root BorderPane
     */
    private Parent getRoot() {
        BorderPane rootPane = new BorderPane();

        rootPane.setTop(getControlBox());
        rootPane.setCenter(getTextArea());
        rootPane.setBottom(getFileStatus());

        return rootPane;
    }

    /**
     * Creates the box with controls on the top of the application's window
     * @return controlBox box with TextField and Buttons.
     */
    private Node getControlBox() {
        HBox controlBox = new HBox();
        TextField keyTf = new TextField();
        Button loadBtn = new Button("Load file");
        Button encryptBtn = new Button("Encrypt");
        FileChooser fileChooser = new FileChooser();
        AES aes = new AES();

        keyTf.setPrefWidth(TEXT_FIELD_WIDTH);
        keyTf.setPromptText("16B key");
        keyTf.setFocusTraversable(false);

        loadBtn.setPrefWidth(BUTTON_WIDTH);
        loadBtn.setOnAction(event -> {
            File selectedFile = fileChooser.showOpenDialog(primaryStage);
            if (selectedFile != null) {
                aes.setFile(selectedFile);
                fileStatus.setText("File successfully loaded");
                encryptBtn.setDisable(false);
            }
        });

        encryptBtn.setPrefWidth(BUTTON_WIDTH);
        encryptBtn.setDisable(true);
        encryptBtn.setOnAction(e -> {
            if(keyTf.getText().length() == 16) {
                aes.setKey(keyTf.getText());
                aes.encrypt();
                textArea.setText(aes.getResults().toString());
                fileStatus.setText("");
            }
            else{
                Alert alert = new Alert(Alert.AlertType.ERROR, "Please select 16 characters long key.");
                alert.show();
            }

        });

        controlBox.setAlignment(Pos.CENTER);
        controlBox.setPadding(new Insets(PADDING));
        controlBox.setSpacing(SPACING);
        controlBox.getChildren().addAll(keyTf, loadBtn, encryptBtn);

        return controlBox;
    }

    /**
     * Creates TextArea for results
     * @return  TextArea
     */
    private Node getTextArea() {
        textArea = new TextArea();
        textArea.setEditable(false);
        textArea.setStyle("-fx-font-size: 1.5em; ");

        return textArea;
    }

    /**
     * Creates text to inform user about selected file at the bottom of the application
     * @return  text
     */
    private Node getFileStatus() {
        fileStatus = new Text();
        fileStatus.setTextAlignment(TextAlignment.CENTER);

        return fileStatus;
    }

}
