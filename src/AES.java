import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * Class represents Advanced Encryption Standard algorithm
 */
public class AES {
    private List<int[][]> roundKeyList;
    private StringBuilder results;
    private File file;
    private String key;
    private static final int FOUR_BYTES = 4;
    private static final int SIXTEEN_BYTES = 16;
    private static final int BIT_MASK = 0xff;

    private final int[][] byteSubstitutionTable = {
            {0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76},
            {0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0},
            {0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15},
            {0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75},
            {0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84},
            {0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf},
            {0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8},
            {0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2},
            {0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73},
            {0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb},
            {0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79},
            {0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08},
            {0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a},
            {0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e},
            {0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf},
            {0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16}
    };

    private final int[][] roundConstMatrix = {
            {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36},
            {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
            {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
            {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
    };

    private final int[] mul2 = {
            0x00, 0x02, 0x04, 0x06, 0x08, 0x0a, 0x0c, 0x0e, 0x10, 0x12, 0x14, 0x16,
            0x18, 0x1a, 0x1c, 0x1e, 0x20, 0x22, 0x24, 0x26, 0x28, 0x2a, 0x2c, 0x2e,
            0x30, 0x32, 0x34, 0x36, 0x38, 0x3a, 0x3c, 0x3e, 0x40, 0x42, 0x44, 0x46,
            0x48, 0x4a, 0x4c, 0x4e, 0x50, 0x52, 0x54, 0x56, 0x58, 0x5a, 0x5c, 0x5e,
            0x60, 0x62, 0x64, 0x66, 0x68, 0x6a, 0x6c, 0x6e, 0x70, 0x72, 0x74, 0x76,
            0x78, 0x7a, 0x7c, 0x7e, 0x80, 0x82, 0x84, 0x86, 0x88, 0x8a, 0x8c, 0x8e,
            0x90, 0x92, 0x94, 0x96, 0x98, 0x9a, 0x9c, 0x9e, 0xa0, 0xa2, 0xa4, 0xa6,
            0xa8, 0xaa, 0xac, 0xae, 0xb0, 0xb2, 0xb4, 0xb6, 0xb8, 0xba, 0xbc, 0xbe,
            0xc0, 0xc2, 0xc4, 0xc6, 0xc8, 0xca, 0xcc, 0xce, 0xd0, 0xd2, 0xd4, 0xd6,
            0xd8, 0xda, 0xdc, 0xde, 0xe0, 0xe2, 0xe4, 0xe6, 0xe8, 0xea, 0xec, 0xee,
            0xf0, 0xf2, 0xf4, 0xf6, 0xf8, 0xfa, 0xfc, 0xfe, 0x1b, 0x19, 0x1f, 0x1d,
            0x13, 0x11, 0x17, 0x15, 0x0b, 0x09, 0x0f, 0x0d, 0x03, 0x01, 0x07, 0x05,
            0x3b, 0x39, 0x3f, 0x3d, 0x33, 0x31, 0x37, 0x35, 0x2b, 0x29, 0x2f, 0x2d,
            0x23, 0x21, 0x27, 0x25, 0x5b, 0x59, 0x5f, 0x5d, 0x53, 0x51, 0x57, 0x55,
            0x4b, 0x49, 0x4f, 0x4d, 0x43, 0x41, 0x47, 0x45, 0x7b, 0x79, 0x7f, 0x7d,
            0x73, 0x71, 0x77, 0x75, 0x6b, 0x69, 0x6f, 0x6d, 0x63, 0x61, 0x67, 0x65,
            0x9b, 0x99, 0x9f, 0x9d, 0x93, 0x91, 0x97, 0x95, 0x8b, 0x89, 0x8f, 0x8d,
            0x83, 0x81, 0x87, 0x85, 0xbb, 0xb9, 0xbf, 0xbd, 0xb3, 0xb1, 0xb7, 0xb5,
            0xab, 0xa9, 0xaf, 0xad, 0xa3, 0xa1, 0xa7, 0xa5, 0xdb, 0xd9, 0xdf, 0xdd,
            0xd3, 0xd1, 0xd7, 0xd5, 0xcb, 0xc9, 0xcf, 0xcd, 0xc3, 0xc1, 0xc7, 0xc5,
            0xfb, 0xf9, 0xff, 0xfd, 0xf3, 0xf1, 0xf7, 0xf5, 0xeb, 0xe9, 0xef, 0xed,
            0xe3, 0xe1, 0xe7, 0xe5
    };

    private final int[] mul3 = {
            0x00, 0x03, 0x06, 0x05, 0x0c, 0x0f, 0x0a, 0x09, 0x18, 0x1b, 0x1e, 0x1d,
            0x14, 0x17, 0x12, 0x11, 0x30, 0x33, 0x36, 0x35, 0x3c, 0x3f, 0x3a, 0x39,
            0x28, 0x2b, 0x2e, 0x2d, 0x24, 0x27, 0x22, 0x21, 0x60, 0x63, 0x66, 0x65,
            0x6c, 0x6f, 0x6a, 0x69, 0x78, 0x7b, 0x7e, 0x7d, 0x74, 0x77, 0x72, 0x71,
            0x50, 0x53, 0x56, 0x55, 0x5c, 0x5f, 0x5a, 0x59, 0x48, 0x4b, 0x4e, 0x4d,
            0x44, 0x47, 0x42, 0x41, 0xc0, 0xc3, 0xc6, 0xc5, 0xcc, 0xcf, 0xca, 0xc9,
            0xd8, 0xdb, 0xde, 0xdd, 0xd4, 0xd7, 0xd2, 0xd1, 0xf0, 0xf3, 0xf6, 0xf5,
            0xfc, 0xff, 0xfa, 0xf9, 0xe8, 0xeb, 0xee, 0xed, 0xe4, 0xe7, 0xe2, 0xe1,
            0xa0, 0xa3, 0xa6, 0xa5, 0xac, 0xaf, 0xaa, 0xa9, 0xb8, 0xbb, 0xbe, 0xbd,
            0xb4, 0xb7, 0xb2, 0xb1, 0x90, 0x93, 0x96, 0x95, 0x9c, 0x9f, 0x9a, 0x99,
            0x88, 0x8b, 0x8e, 0x8d, 0x84, 0x87, 0x82, 0x81, 0x9b, 0x98, 0x9d, 0x9e,
            0x97, 0x94, 0x91, 0x92, 0x83, 0x80, 0x85, 0x86, 0x8f, 0x8c, 0x89, 0x8a,
            0xab, 0xa8, 0xad, 0xae, 0xa7, 0xa4, 0xa1, 0xa2, 0xb3, 0xb0, 0xb5, 0xb6,
            0xbf, 0xbc, 0xb9, 0xba, 0xfb, 0xf8, 0xfd, 0xfe, 0xf7, 0xf4, 0xf1, 0xf2,
            0xe3, 0xe0, 0xe5, 0xe6, 0xef, 0xec, 0xe9, 0xea, 0xcb, 0xc8, 0xcd, 0xce,
            0xc7, 0xc4, 0xc1, 0xc2, 0xd3, 0xd0, 0xd5, 0xd6, 0xdf, 0xdc, 0xd9, 0xda,
            0x5b, 0x58, 0x5d, 0x5e, 0x57, 0x54, 0x51, 0x52, 0x43, 0x40, 0x45, 0x46,
            0x4f, 0x4c, 0x49, 0x4a, 0x6b, 0x68, 0x6d, 0x6e, 0x67, 0x64, 0x61, 0x62,
            0x73, 0x70, 0x75, 0x76, 0x7f, 0x7c, 0x79, 0x7a, 0x3b, 0x38, 0x3d, 0x3e,
            0x37, 0x34, 0x31, 0x32, 0x23, 0x20, 0x25, 0x26, 0x2f, 0x2c, 0x29, 0x2a,
            0x0b, 0x08, 0x0d, 0x0e, 0x07, 0x04, 0x01, 0x02, 0x13, 0x10, 0x15, 0x16,
            0x1f, 0x1c, 0x19, 0x1a
    };

    /**
     * Constructor
     */
    public AES() {
    }

    /**
     * Algorithm which encrypts data
     */
    public void encrypt() {
        roundKeyList = new ArrayList<>();                               //list of round keys
        results = new StringBuilder();                                  //StringBuilder with results as hexadecimal Strings
        ArrayList<Integer> stateList = new ArrayList<Integer>();        //ArrayList for keeping state of hex. data
        byte[] kb = this.key.getBytes();
        byte[] fb = fileToByte(this.file);

        int lengthAlign = fb.length;
        if (fb.length % SIXTEEN_BYTES != 0) {                           //computing of how much to expand array of
            lengthAlign = ((fb.length / SIXTEEN_BYTES) + 1) * SIXTEEN_BYTES;    //filebytes to align them for 16 multiple
        }
        int[] fileBytes = new int[lengthAlign];
        for (int i = 0; i < fb.length; i++) {
            fileBytes[i] = fb[i] & BIT_MASK;                            //using bitmask to get right bites
        }

        int counter = 0;
        for (int k = 0; k < fileBytes.length; k++) {
            stateList.add(fileBytes[k]);
            counter++;

            if (counter == SIXTEEN_BYTES) {                             //getting 16 byte chunk of data
                int[][] state = makeStateArray(stateList);              //converting ArrayList to 2d array

                int[][] key = new int[FOUR_BYTES][FOUR_BYTES];
                int index = 0;
                for (int i = 0; i < key.length; i++) {
                    for (int j = 0; j < key.length; j++) {
                        key[i][j] = kb[index++] & BIT_MASK;             //converting bytes to 2d array
                    }
                }

                roundKeyList.add(key);                                  //creating of keys, added orig. key to first
                for (int i = 0; i < 10; i++) {                          //index
                    makeRoundKey(roundKeyList.get(i), i);
                }

                addRoundKey(state, roundKeyList.get(0));               // xor of state (input) with orig. key

                for (int i = 1; i <= 10; i++) {
                    subBytes(state);
                    shiftRows(state);
                    if (i < 10) {                                      //in round 10, there is no mixColumns
                        mixColumns(state);
                    }
                    addRoundKey(state, roundKeyList.get(i));
                }

                writeResults(state);
                stateList.clear();
                counter = 0;
            }
        }
    }

    /**
     * Setter for file, which is chosen by user
     * @param file  chosen file
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * Setter for key, which is chosen by user
     * @param key chosen key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Method converts file to bytes
     * @param file file to convert
     * @return  array of bytes
     */
    private byte[] fileToByte(File file) {
        byte[] fb = null;
        try {
            fb = Files.readAllBytes(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fb;
    }

    /**
     * Method converts ArrayList of Integers to 2D array of ints.
     * @param list  ArrayList to convert
     * @return  2D converted array
     */
    private int[][] makeStateArray(ArrayList<Integer> list) {
        int[][] state = new int[FOUR_BYTES][FOUR_BYTES];

        state[0][0] = list.get(0);
        state[0][1] = list.get(1);
        state[0][2] = list.get(2);
        state[0][3] = list.get(3);

        state[1][0] = list.get(4);
        state[1][1] = list.get(5);
        state[1][2] = list.get(6);
        state[1][3] = list.get(7);

        state[2][0] = list.get(8);
        state[2][1] = list.get(9);
        state[2][2] = list.get(10);
        state[2][3] = list.get(11);

        state[3][0] = list.get(12);
        state[3][1] = list.get(13);
        state[3][2] = list.get(14);
        state[3][3] = list.get(15);

        return state;
    }


    /**
     * Method do xor with each column of result array with columns of roundKey array
     * @param result    result array where results are written
     * @param roundKey  round key
     */
    private void addRoundKey(int[][] result, int[][]roundKey) {
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result.length; j++) {
                result[i][j] = result[i][j] ^ roundKey[i][j];
            }
        }
    }

    /**
     * Method performs mix columns, which means multiplication of state matrix and specific matrix for
     * mix column algorithm. It is not normal matrix multiplication, multiply is replaced with Galois
     * multiplication and addition is replaced with xor.
     * @param state matrix to mix
     */
    private void mixColumns(int[][] state) {
        int[][] tmp = new int[FOUR_BYTES][FOUR_BYTES];

        for (int i = 0; i < 4; i++) {
            tmp[i][0] = mul2[state[i][0]] ^ mul3[state[i][1]] ^ state[i][2] ^ state[i][3];
            tmp[i][1] = state[i][0] ^ mul2[state[i][1]] ^ mul3[state[i][2]] ^ state[i][3];
            tmp[i][2] = state[i][0] ^ state[i][1] ^ mul2[state[i][2]] ^ mul3[state[i][3]];
            tmp[i][3] = mul3[state[i][0]] ^ state[i][1] ^ state[i][2] ^ mul2[state[i][3]];
        }

        for (int i = 0; i < tmp.length; i++) {
            for (int j = 0; j < tmp[i].length; j++) {
                state[j][i] = tmp[j][i];
            }
        }
    }

    /**
     * Method shifts array of bytes in a sense that second row shifts to right side by one, third by two
     * and fourth by three. First row is unchanged
     * @param bytesToShift  array of bytes to shift
     */
    private void shiftRows(int[][] bytesToShift) {
        int[][]clone = new int[bytesToShift.length][bytesToShift.length];
        for (int i = 0; i < bytesToShift.length; i++) {
            for (int j = 0; j < bytesToShift[i].length; j++) {
                clone[j][i] = bytesToShift[j][i];
            }
        }

        bytesToShift[3][1] = clone[0][1];           //first row
        bytesToShift[2][1] = clone[3][1];
        bytesToShift[1][1] = clone[2][1];
        bytesToShift[0][1] = clone[1][1];

        bytesToShift[3][2] = clone[1][2];           //second row
        bytesToShift[2][2] = clone[0][2];
        bytesToShift[1][2] = clone[3][2];
        bytesToShift[0][2] = clone[2][2];

        bytesToShift[3][3] = clone[2][3];           //third row
        bytesToShift[2][3] = clone[1][3];
        bytesToShift[1][3] = clone[0][3];
        bytesToShift[0][3] = clone[3][3];
    }

    /**
     * Method makes round key from previous key, which is given in argument
     * @param key       key from which next round key will be created
     * @param number    number for right usage of xor with roundConstMatrix
     */
    private void makeRoundKey (int[][]key, int number) {
        int[][] roundKey = new int[FOUR_BYTES][FOUR_BYTES];
        int[] rotWord = new int[FOUR_BYTES];

        rotWord[0] = key[3][1];                               //first row and reverse
        rotWord[1] = key[3][2];
        rotWord[2] = key[3][3];
        rotWord[3] = key[3][0];

        subColumn(rotWord);                                    //substitution

        for (int i = 0; i < 4; i++) {
            roundKey[0][i] = key[0][i] ^ rotWord[i] ^ roundConstMatrix[i][number];
            roundKey[1][i] = key[1][i] ^ roundKey[0][i];
            roundKey[2][i] = key[2][i] ^ roundKey[1][i];
            roundKey[3][i] = key[3][i] ^ roundKey[2][i];
        }

        roundKeyList.add(roundKey);
    }

    /**
     * Method for substitution of whole 2D byte array
     * @param bytesToSub    2D byte array to substitute
     */
    private  void subBytes(int[][] bytesToSub) {
        int[] firstCol = {
                bytesToSub[0][0], bytesToSub[0][1], bytesToSub[0][2], bytesToSub[0][3]
        };
        int[] secondCol = {
                bytesToSub[1][0], bytesToSub[1][1], bytesToSub[1][2], bytesToSub[1][3]
        };
        int[] thirdCol = {
                bytesToSub[2][0], bytesToSub[2][1], bytesToSub[2][2], bytesToSub[2][3]
        };
        int[] fourthCol = {
                bytesToSub[3][0], bytesToSub[3][1], bytesToSub[3][2], bytesToSub[3][3]
        };

        subColumn(firstCol);
        subColumn(secondCol);
        subColumn(thirdCol);
        subColumn(fourthCol);

        for (int i = 0; i < firstCol.length; i++) {
            bytesToSub[0][i] = firstCol[i];
        }
        for (int i = 0; i < secondCol.length; i++) {
            bytesToSub[1][i] = secondCol[i];
        }
        for (int i = 0; i < thirdCol.length; i++) {
            bytesToSub[2][i] = thirdCol[i];
        }
        for (int i = 0; i < fourthCol.length; i++) {
            bytesToSub[3][i] = fourthCol[i];
        }
    }

    /**
     * Method to subtitute only one column of bytes
     * @param columnToSub   array of column to substite
     */
    private void subColumn(int[] columnToSub) {
        String s1, s2;
        for (int i = 0; i < columnToSub.length; i++) {
            if (Integer.toHexString(columnToSub[i]).length() == 1) {
                s1 = "0";
                s2 = Integer.toHexString(columnToSub[i]).charAt(0) + "";
            }
            else {
                s1 = Integer.toHexString(columnToSub[i]).charAt(0) + "";
                s2 = Integer.toHexString(columnToSub[i]).charAt(1) + "";
            }

            int decimal1=Integer.parseInt(s1,SIXTEEN_BYTES);
            int decimal2=Integer.parseInt(s2,SIXTEEN_BYTES);

            columnToSub[i] = byteSubstitutionTable[decimal1][decimal2];
        }

    }

    /**
     * Method adds result from 2D byte array to StringBuilder where are represented as hexadecimal String
     * @param result    2D byte array
     */
    private void writeResults(int result[][]) {
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                if (Integer.toHexString(result[i][j]).length() == 1) {
                    results.append("0" + Integer.toHexString(result[i][j]) + " ");
                } else {
                    results.append(Integer.toHexString(result[i][j]) + " ");
                }
            }
        }
        results.append("\n");
    }


    /**
     * Getter for results
     * @return  results
     */
    public StringBuilder getResults() {
        return results;
    }
}
